#!/usr/bin/env bash

#init
RUBY_VERSION="2.4.0"
VAGRANT_VERSION="2.1.2"

set -eo pipefail

if [[ $(whoami) == 'root' ]]; then
  SUDO=""
else
  SUDO=sudo
fi

#statements
#update #checked on July 19 2018 by Mohamed
echo "Update distribution"
${SUDO} apt-get update -y
${SUDO} apt-get upgrade -y
${SUDO} apt-get -f install
${SUDO} apt --fix-broken install

#Zsh #checked on July 19 2018 by Mohamed
echo "Install vim curl wget zsh git"
${SUDO} apt-get install -y vim curl wget zsh git
chsh -s $(which zsh)

#OhMyZSH ##checked on July 19 2018 by Mohamed
echo "Install OhMyZSH and some plugins"
if [[ ! -e ~/.oh-my-zsh ]]; then #Ignore Install if dir exist
  ~/dotfiles-local/tools/ohmyzsh-install.sh
fi
#OMZ plugin #checked on July 19 2018 by Mohamed
cd ~/.oh-my-zsh/custom/plugins
if [[ ! -e ~/.zsh-autosuggestions ]]; then #Ignore Install if dir exist
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
  git clone git://github.com/zsh-users/zsh-autosuggestions
  git clone https://github.com/tarruda/zsh-autosuggestions ~/.zsh-autosuggestions
fi

# fonts #checked on July 19 2018 by Mohamed
echo "Install some fonts"
${SUDO} apt install -y gsfonts-x11

#Powerline font #checked on July 19 2018 by Mohamed
mkdir -p ~/.fonts/ && cd ~/.fonts/
wget https://github.com/Lokaltog/powerline/raw/develop/font/PowerlineSymbols.otf
mkdir -p ~/.config/fontconfig/conf.d/ && cd ~/.config/fontconfig/conf.d/
wget https://github.com/Lokaltog/powerline/raw/develop/font/10-powerline-symbols.conf

#OMZ themes #checked on July 19 2018 by Mohamed
echo "Install MyOMZ theme"
/bin/cp -rf ~/dotfiles-local/oh-my-zsh-themes/* $HOME/.oh-my-zsh/themes/

#rcm #checked on July 19 2018 by Mohamed
echo "Install rcm and thoughtbot/dotfiles"
wget -qO - https://apt.thoughtbot.com/thoughtbot.gpg.key | ${SUDO} apt-key add -
echo "deb http://apt.thoughtbot.com/debian/ stable main" | ${SUDO} tee /etc/apt/sources.list.d/thoughtbot.list
${SUDO} apt-get update
${SUDO} apt-get install -y rcm
cd
if [ -d dotfiles ]; then
  cd dotfiles
  git pull
else
   git clone https://github.com/thoughtbot/dotfiles.git
fi
cd
env RCRC=$HOME/dotfiles/rcrc rcup -x tools
#Dev tools #checked on July 19 2018 by Mohamed
echo "Install dev tools and dependencies"
${SUDO} apt-get install -y htop dfc git-flow python-pip libssl-dev libreadline-dev zlib1g-dev

#Rbenv #checked on July 19 2018 by Mohamed
echo "Install rbenv"
if [[ ! -e ~/.rbenv ]]; then
  cd
  git clone https://github.com/rbenv/rbenv.git ~/.rbenv
  git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
  git clone https://github.com/sstephenson/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash
  export PATH="$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"
  rbenv install -l |grep $RUBY_VERSION && \
  rbenv install $RUBY_VERSION && \
  rbenv global $RUBY_VERSION
fi

#Tmux #checked on July 19 2018 by Mohamed
echo "Install tmux & tmate"
${SUDO} apt-get install -y tmux
export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
gem install tmuxinator

#tmate.io #checked on July 19 2018 by Mohamed
${SUDO} apt-get install -y software-properties-common && \
${SUDO} apt-get install -y tmate

#Docker #checked on July 19 2018 by Mohamed
echo "Install docker"
docker -v || curl -fsSL https://get.docker.com/ | sh
${SUDO} apt --fix-broken install

#Docker-compose
echo "Install Docker Compose"
${SUDO} curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
${SUDO} chmod +x /usr/local/bin/docker-compose

#Vagrant #checked on July 19 2018 by Mohamed
echo "Install vagrant"
mkdir -p /tmp/install_vagrant
cd /tmp/install_vagrant
wget https://releases.hashicorp.com/vagrant/${VAGRANT_VERSION}/vagrant_${VAGRANT_VERSION}_x86_64.deb
${SUDO} dpkg -i vagrant_*.deb
cd /tmp && rm -rf /tmp/install_vagrant

#Virtualbox #checked on July 19 2018 by Mohamed
echo "Install virtualbox"
${SUDO} apt-get install -y libsdl1.2debian libqt5x11extras5 libopus0 libqt5opengl5 libqt5printsupport5 libvpx5 libxmu6 libxt6
mkdir -p /tmp/install_virtualbox
cd /tmp/install_virtualbox
wget https://download.virtualbox.org/virtualbox/5.2.16/virtualbox-5.2_5.2.16-123759~Ubuntu~bionic_amd64.deb
${SUDO} dpkg -i virtualbox*.deb
cd /tmp && rm -rf /tmp/install_virtualbox

#My bins #checked on July 19 2018 by Mohamed
echo "Install My bin"
${SUDO} apt-get install -y asciinema

#Dropbox #checked on July 19 2018 by Mohamed
echo "Install Dropbox"
mkdir -p /tmp/install_dropbox
cd /tmp/install_dropbox
wget https://www.dropbox.com/download?dl=packages/ubuntu/dropbox_2015.10.28_amd64.deb
${SUDO} apt-get install -y libpango1.0-0 libgtk2.0-0 python-gtk2
sudo dpkg -i *.deb
cd /tmp && rm -rf /tmp/install_dropbox
dropbox start -i

#My dev dir #checked on July 19 2018 by Mohamed
echo "Create my dev dir"
cd ~
mkdir -p dev/clients
mkdir -p dev/app
mkdir -p dev/env
mkdir -p dev/tmp

#variety #checked on July 19 2018 by Mohamed
echo "Install variety"
${SUDO} apt-get update
${SUDO} apt-get install -y variety

#Nemo #checked on July 19 2018 by Mohamed
echo "Install nemo"
#echo -e "\r"|${SUDO} add-apt-repository ppa:webupd8team/nemo3
${SUDO} apt-get update
${SUDO} apt-get install -y nemo
xdg-mime default nemo.desktop inode/directory application/x-gnome-saved-search #add nemo as default

#Rambox (chat client) #checked on July 19 2018 by Mohamed
echo "Install Rambox"
 if [[ ! -e /usr/local/bin/rambox.appimage ]]; then
   mkdir -p /tmp/ramboxappimage
   cd /tmp/ramboxappimage
   wget https://github.com/saenzramiro/rambox/releases/download/0.5.3/Rambox-0.5.3-x64.AppImage
   ${SUDO}  chmod +x Rambox-0.5.3-x64.AppImage
   ${SUDO} chown -R root:root Rambox-0.5.3-x64.AppImage
   ${SUDO} mv Rambox-0.5.3-x64.AppImage /usr/local/bin/rambox.appimage
   cd ~
   rm -Rf /tmp/ramboxappimage
fi

#multi-system #checked on July 19 2018 by Mohamed
echo "Install multisystem"
wget -q -O - http://liveusb.info/multisystem/depot/multisystem.asc | ${SUDO} apt-key add -
${SUDO} apt-add-repository 'deb http://liveusb.info/multisystem/depot all main'
${SUDO} apt-get update
${SUDO} apt-get --assume-yes install multisystem

#TODO creer un virtualenv pour python et pip #checked on July 19 2018 by Mohamed
#ansible
echo "Install ansible"
#pip install --upgrade pip --user
${SUDO} pip install ansible ansible-lint

#Atom #checked on July 21 2018 by Mohamed
echo "Install Atom and some useful plugins"
mkdir -p /tmp/install_atom
cd /tmp/install_atom
wget https://atom.io/download/deb -O atom.deb
${SUDO} apt-get install -y gconf2 gconf-service libxss1
${SUDO} dpkg -i *.deb
${SUDO} apt-get install -f
cd /tmp && rm -rf /tmp/install_atom
apm install language-terraform
apm install language-tmux
apm install language-ansible
apm install file-icons
apm install tree-view-git-status
apm install ansible-vault
apm install linter-ansible-linting
apm install Linter-Ansible-Syntax
apm install ansible-snippets
apm install autocomplete-ansible
apm install railscast-theme
apm install merge-conflicts
apm install indent-tooltip
apm install indent-guide-improved
apm install auto-indent

#DNS utils #checked on July 19 2018 by Mohamed
echo "Install DNS utils"
${SUDO} apt-get install -y dnsutils

#Antivirus #checked on July 19 2018 by Mohamed
echo "Install clamav antivirus"
${SUDO} apt-get update
${SUDO} apt-get install -y clamav clamav-daemon clamtk

# Install synology backup script
echo "Install synology backup script"
${SUDO} ln -s ~/dotfiles-local/bin/backup2DSM.sh /usr/local/bin

echo "Installation terminée."
echo "Nous vous invitons à redémarrer votre machine pour appliquer les modifications."
